@extends('layout.navbar')
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
@section('container')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Barang</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{route('barang.simpan')}}" method="post">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Barang</label>

                  <div class="col-sm-5">
                    <input type="text" name="nama" required="required" class="form-control" id="inputEmail3" placeholder="Nama Barang">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jumlah</label>

                  <div class="col-sm-5">
                    <input type="number" name="jumlah" required="required" class="form-control" id="inputPassword3" placeholder="Jumlah Barang">
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-left fa fa-save"> Simpan Data</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div> 
    </section>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   @endsection