@extends('layout.navbar')
@section('container')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  

  <!-- Main content -->
  <section class="content container-fluid">
  <!-- Content Wrapper. Contains page content -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title col-sm-5">Edit Data Barang</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      @foreach($barang as $p)
      <form class="form-horizontal" action="{{route('barang.sudah_edit')}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $p->id }}"> <br/>
        <div class="box-body">
          <div class="form-group">
            <label for="inputBarang" class="col-sm-3 control-label">Nama Barang</label>

            <div class="col-sm-6">
              <input type="text" required="required" class="form-control" id="inputBarang" value="{{ $p->nama }}" name="nama">
            </div>
          </div>
          <div class="form-group">
            <label for="inputJumlah" class="col-sm-3 control-label">Jumlah Barang</label>

            <div class="col-sm-6">
              <input type="number" required="required" class="form-control" id="inputJumlah" value="{{$p->jumlah}}" name="jumlah">
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info">Simpan Data</button>
        </div>
        <!-- /.box-footer -->
      </form>
      @endforeach
    </div>
  </section>
</div>
  

   @endsection