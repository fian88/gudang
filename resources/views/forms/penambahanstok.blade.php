@extends('layout.navbar')
@section('container')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  

  <!-- Main content -->
  <section class="content container-fluid">
  <!-- Content Wrapper. Contains page content -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title col-sm-5">Tambah Stok Barang</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      @foreach($barang as $p)
      <form class="form-horizontal" action="{{route('stok_barang.sudah_ditambah')}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $p->id }}"> <br/>
        <input type="hidden" name="nama" value="{{ $p->nama }}"> <br/>
        <div class="box-body">
          <div class="form-group">
            <label for="inputJumlah" class="col-sm-3 control-label">Tambahan Stok</label>

            <div class="col-sm-6">
              <input type="number" required="required" class="form-control" id="inputJumlah" name="jumlah">
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info">Simpan Data</button>
        </div>
        <!-- /.box-footer -->
      </form>
      @endforeach
    </div>
  </section>
</div>


@endsection