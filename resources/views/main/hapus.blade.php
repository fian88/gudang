@extends('layout.navbar')


@section('container')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content">
      <div class="row">
        <div class="col-xs-8">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilih Barang yang Dihapus</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12">
                <table id="barang" class="table table-bordered" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Nama</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Jumlah</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Opsi</th></tr>
                </thead>
                <tbody>
                @foreach($barang as $p)
                <tr role="row" class="odd">
                  <td class="sorting_1">{{ $p->id }}</td>
                  <td>{{ $p->nama }}</td>
                  <td>{{ $p->jumlah }}</td>
                  <td>
                    <a href="{{ route('barang.sudah_dihapus', [$p->id]) }}" class="btn btn-block btn-danger fa fa-trash"> Hapus</a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table></div></div>
              {{-- {{ $barang->links() }} --}}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        
    </section>
    <!-- /.content -->
  </div>

 
  <!-- Control Sidebar -->
   @endsection