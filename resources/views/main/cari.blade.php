@extends('main.index')

@section('cari')


  <!-- Content Wrapper. Contains page content -->
  {{-- <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Barang Tersedia</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="/cari" method="GET">
                <input type="text" name="cari" placeholder="Cari Barang .." value="{{ old('cari') }}">
                <input type="submit" value="CARI">
              </form>
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12">
                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Nama</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Jumlah</th></tr>
                </thead>
                <tbody>
                @foreach($barang as $p)
                <tr role="row" class="odd">
                  <td class="sorting_1">{{ $p->id }}</td>
                  <td>{{ $p->nama }}</td>
                  <td>{{ $p->jumlah }}</td>
                </tr>
                @endforeach
                </tbody>
                <tr>
                  <td><a href="/" class="btn btn-block btn-danger">Kembali </a></td>
                </tr>
              </table></div></div>
               {{ $barang->links() }}
               
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        
    <!-- /.content -->
  </div> --}}

  <td><a href="/" class="btn btn-warning mr-5 fa fa-arrow-left"> Kembali</a></td>
  

  <!-- Control Sidebar -->
  @endsection