@extends('layout.navbar')

@section('container')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content">
      <div class="row">
        <div class="col-xs-8">
          <div class="box">
            @yield('cari')
            <div class="box-header">
              <h3 class="box-title">Daftar Barang Tersedia</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              {{-- cari barang --}}
              {{-- <form action="/cari" method="GET">
                <input type="text" name="cari" placeholder="Cari Barang .." value="{{ old('cari') }}">
                <button type="submit" class="fa fa-search"> Cari</button>
              </form> --}}
              {{-- cari id --}}
              {{-- <form action="/cari_id" method="GET">
                <input type="text" name="cari" placeholder="Cari ID .." value="{{ old('cari') }}">
                <button type="submit" class="fa fa-search"> Cari</button>
              </form> --}}

              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12">
                <table id="barang" class="table table-bordered" role="grid" aria-describedby="example2_info">
                  <thead>
                    <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Nama</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Jumlah</th></tr>
                  </thead>
                  <tbody>
                    {{-- <?php                 
                    // $counter=1
                    ?> --}}
                    @foreach($barang as $p)
                    <tr role="row" class="odd">
                      <td class="sorting_1">{{ $p->id }}</td>
                      <td>{{ $p->nama }}</td>
                      <td>{{ $p->jumlah }}</td>
                    </tr>
                    {{-- <?php 
                    // $counter = $counter +1;
                    ?> --}}
                    @endforeach
                  </tbody>

                </table></div></div>
                {{-- {{ $barang->links() }} --}}
                
              </br>
              </br>
                <div class="container">
                  <a href="{{ route('barang.print') }}" class="btn btn-success my-3 fa fa-print" target="_blank"> PRINT</a>
                  <a href="{{ route('barang.eksport') }}" class="btn btn-success my-3 fa fa-download" target="_blank"> EXPORT EXCEL</a>
              
                  <button type="button" class="btn btn-primary mr-5 fa fa-upload" data-toggle="modal" data-target="#importExcel">
                    IMPORT EXCEL
                  </button>

                  <button type="button" class="btn btn-warning fa fa-plus mr-5" data-toggle="modal" data-target="#tambahBarang">
                    TAMBAH BARANG
                  </button>
                </div>
              </div>
            
              <!--/.Tambah Barang -->
            <div class="modal fade" id="tambahBarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <form method="post" action="{{ route('barang.simpan') }}" enctype="multipart/form-data">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Barang</h5>
                    </div>
                      <div class="modal-body">
        
                      {{ csrf_field() }}
                      <div class="box-body">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Nama Barang</label>
                          <div class="col-sm-6">
                            <input type="text" name="nama" required="required" class="form-control" id="inputEmail3" placeholder="Nama Barang">
                          </div>
                        </div>
                        <br/>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Jumlah</label>
                          <div class="col-sm-6">
                            <input type="number" name="jumlah" required="required" class="form-control" id="inputPassword3" placeholder="Jumlah Barang">
                          </div>
                        </div>
                        
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-left">Simpan Data</button>
                      </div>
         
                    </div>
                  </div>
                </form>
              </div>
            </div>
            
            <!--/.import barang -->
            <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <form method="post" action="{{ route('barang.import') }}" enctype="multipart/form-data">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                    </div>
                    <div class="modal-body">
         
                      {{ csrf_field() }}
         
                      <label>Pilih file excel</label>
                      <div class="form-group">
                        <input type="file" name="file" required="required">
                      </div>
         
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
 

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        
    </section>
    <!-- /.content -->
  </div>

  <!--/.excel export -->


  <!-- Control Sidebar -->
  @endsection