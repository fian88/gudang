<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use app\Barang;

use App\Imports\BarangImport;
use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class BarangController extends Controller
{
    public function index()
    {
    	// mengambil data dari table pegawai
    	$barang = DB::table('barang')->get();
 
    	// mengirim data pegawai ke view index
    	return view('main.index',[
		    'barang' => $barang, 
		    'title' => "Pengolah Barang"]);
 
    }
    public function print()
    {
    	// mengambil data dari table pegawai
    	$barang = DB::table('barang')->get();
 
    	// mengirim data pegawai ke view index
    	return view('layout.print',[
		    'barang' => $barang]);
 
    }
    // method untuk menampilkan view form tambah pegawai
	public function tambahbarang()
	{
	 	// memanggil view tambah
		return view('forms.tambahbarang', [
		'title' => "Tambah Barang"]);
	 
	}
	// method untuk insert data ke table pegawai
	public function store(Request $request)
	{
		// insert data ke table pegawai
		DB::table('barang')->insert([
			'nama' => $request->nama,
			'jumlah' => $request->jumlah,
		]);
		// alihkan halaman ke halaman pegawai
		return redirect()->route('barang.index');
	 
	}
	// method untuk edit data pegawai
	public function edit($id)
	{
		// mengambil data pegawai berdasarkan id yang dipilih
		$barang = DB::table('barang')->where('id',$id)->get();
		// passing data pegawai yang didapat ke view edit.blade.php
		return view('form.ubah',[
			'barang' => $barang,
			'title' => "Edit Barang"]);
	 
	}
	// update data pegawai
	// update data pegawai
	public function update(Request $request)
	{
		// update data pegawai
		DB::table('barang')->where('id',$request->id)->update([
			'nama' => $request->nama,
			'jumlah' => $request->jumlah
		]);
		// alihkan halaman ke halaman pegawai
		return redirect(route('barang.pilih_edit'));
	}
	// method untuk hapus data pegawai
	public function hapus($id)
	{
		// menghapus data pegawai berdasarkan id yang dipilih
		DB::table('barang')->where('id',$id)->delete();
			
		// alihkan halaman ke halaman pegawai
		return redirect()->route('barang.pilih_hapus');
	}
	public function ubah()
	{
	 
	// mengambil data dari table pegawai
   	$barang = DB::table('barang')->get();
 
   	// mengirim data pegawai ke view index
   	return view('main.ubah',[
		'barang' => $barang,
		'title' => "Ubah Data Barang"]);
	 
	}
	public function lamanHapus()
	{
	 
	// mengambil data dari table pegawai
   	$barang = DB::table('barang')->get();
 
   	// mengirim data pegawai ke view index
   	return view('main.hapus',[
		'barang' => $barang,
		'title' => "Hapus Data Barang"]);
	}


	// public function cari(Request $request)
	// {
	// 	// menangkap data pencarian
	// 	$cari = $request->cari;
 
    	// 	// mengambil data dari table pegawai sesuai pencarian data
	// 	$barang = DB::table('barang')
	// 	->where('nama','like',"%".$cari."%")
	// 	->paginate(5);
 
    	// 	// mengirim data pegawai ke view index
	// 	// return view('main.cari',[
	// 	// 	'barang' => $barang,
	// 	// 	'title' => "Cari Barang"]);

	// 	return view('main.index',[
	// 		'barang'=>$barang,
	// 		'title' => "Hasil Pencarian"]);
 
	// }

	// public function cari_id(Request $request)
	// {
	// 	// menangkap data pencarian
	// 	$cari = $request->cari;
 
    	// 	// mengambil data dari table pegawai sesuai pencarian data
	// 	$barang = DB::table('barang')
	// 	->where('id','like',"%".$cari."%")
	// 	->paginate(5);
		
    	// 	// mengirim data pegawai ke view index
	// 	return view('main.cari',[
	// 		'barang' => $barang,
	// 		'title' => "Cari Barang"]);

	// 	// return view('main.index',[
	// 	// 	'barang'=>$barang,
	// 	// 	'title' => "Hasil Pencarian"]);
 
	// }

	// eksport excel
	public function export_excel()
	{
		return Excel::download(new BarangExport, 'barang.xlsx');
	}

	// import excel
	public function import_excel(Request $request) 
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file_barang',$nama_file);
 
		// import data
		Excel::import(new BarangImport, public_path('/file_barang/'.$nama_file));
 
		// alihkan halaman kembali
		return redirect()->route('barang.index');
	}

	public function tambahstok()
	{
	 
	// mengambil data dari table pegawai
   	$barang = DB::table('barang')->get();
 
   	// mengirim data pegawai ke view index
   	return view('main.tambahstok',[
		'barang' => $barang,
		'title' => "Tambah Stok Barang"]);
	 
	}
	
	public function penambahanstok($id)
	{
		// mengambil data pegawai berdasarkan id yang dipilih
		$barang = DB::table('barang')->where('id',$id)->get();
		// passing data pegawai yang didapat ke view edit.blade.php
		return view('forms.penambahanstok',[
			'barang' => $barang,
			'title' => "Tambah Stok Barang"]);
	 
	}
	// update data pegawai
	// update data pegawai
	public function updatestok(Request $request)
	{

		$barang = DB::table('barang')->where('id',$request->id)->get();
		foreach($barang as $p){
			$stok1 = $request->jumlah;
			$stok2 = $p->jumlah;
		};
		
		$jumlah = $stok1 + $stok2;
		// update data pegawai
		DB::table('barang')->where('id',$request->id)->update([
			'nama' => $request->nama,
			'jumlah' => $jumlah 
		]);
	// alihkan halaman ke halaman pegawai
		return redirect()->route('stok_barang.pilih_tambah');
	}

	public function kurangistok()
	{
	 
	// mengambil data dari table pegawai
   	$barang = DB::table('barang')->get();
 
   	// mengirim data pegawai ke view index
   	return view('main.kurangistok',[
		'barang' => $barang,
		'title' => "Kurangi Stok Barang"]);
	 
	}
	
	public function penguranganstok($id)
	{
		// mengambil data pegawai berdasarkan id yang dipilih
		$barang = DB::table('barang')->where('id',$id)->get();
		// passing data pegawai yang didapat ke view edit.blade.php
		return view('forms.penguranganstok',[
			'barang' => $barang,
			'title' => "Kurangi Stok Barang"]);
	 
	}
	// update data pegawai
	// update data pegawai
	public function mengurangistok(Request $request)
	{
		$barang = DB::table('barang')->where('id',$request->id)->get();
		foreach($barang as $p){
			$stok1 = $request->jumlah;
			$stok2 = $p->jumlah;
		};
		
		$jumlah = $stok2 - $stok1;
		// update data pegawai
		DB::table('barang')->where('id',$request->id)->update([
			'nama' => $request->nama,
			'jumlah' => $jumlah 
		]);
	// alihkan halaman ke halaman pegawai
		return redirect()->route('stok_barang.pilih_kurang');
	}
}