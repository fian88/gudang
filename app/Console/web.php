<?php


Route::get('/welcome', function () {
    return view('master.admin_kayu');
});

Route::get('/banding', 'MenuController@banding_kayu');

Route::get('/', 'LoginController@login')->name('login');
Route::post('login', 'LoginController@post_login')->name('login.post');
// Route::get('/add', 'LoginController@add_user')->name('l');

Route::get('logout', 'LoginController@logout')->name('logout');

// Auth::routes();

// Route::get('dashboard', 'KayuController@dashboard')->name('dashboard_admin.index');
Route::group(['prefix' => 'kayoe'], function () {
    // Dashboard

    // Surat Penerimaan
    Route::get('penerimaan', 'KayuController@penerima')->name('penerima.index');
    Route::get('supplier', 'KayuController@supplier')->name('admin.supplier');
    Route::get('datatable-supplier', 'KayuController@datatable_supplier')->name('supplier.datatable');
    Route::post('save-ajax', 'KayuController@simpan_data')->name('simpan_data.save');
    Route::get('form_edit/{id}', 'KayuController@form_edit')->name('supplier.form_edit');
    
    //Form Input Kayu
    Route::get('form', 'KayuController@form')->name('admin.form');
    Route::post('simpan-form', 'KayuController@simpan_form')->name('simpan.form');
    Route::post('datatable-form', 'KayuController@datatable_form')->name('form.datatable');
    Route::post('hapus-harga', 'KayuController@hapus_harga')->name('hapus_harga.delete');
    Route::post('atur-harga', 'KayuController@atur_harga')->name('cari.harga');
    Route::post('simpan-harga', 'KayuController@simpan_harga')->name('simpan_harga.save');
    Route::get('max-seri', 'KayuController@get_seri')->name('get_seri.json');

    Route::post('datatable-belum-acc', 'KayuController@datatable_belum_acc')->name('belum_acc.datatable');
    Route::post('konfirmasi-harga', 'KayuController@konfirmasi_harga')->name('konfirmasi.harga');
    
    //Kayu Masuk
    Route::get('data-supplier', 'KayuController@data_supplier')->name('data_supplier.index');
    Route::get('list-supplier', 'KayuController@list_supplier')->name('supplier_list.datatable');
    Route::post('simpan', 'KayuController@simpan')->name('kayu.simpan');
    Route::post('get-kayu', 'KayuController@get_kayu')->name('get_kayu.json');
    Route::post('add-item', 'KayuController@add_item')->name('add_item.save');

    Route::post('set-pot-kubik', 'KayuController@set_pot_kubik')->name('set.kubik');
    Route::post('get-pot-kubik', 'KayuController@get_pot_kubik')->name('get.kubik');

    
    // Route::get('form_baru', 'KayuController@form_baru')->name('form.baru');
    Route::post('ganti-tanggal', 'KayuController@ganti_tanggal')->name('simpan.tanggal');
    Route::post('get-harga', 'KayuController@get_harga')->name('harga.get');
    Route::post('delete-item-detail', 'KayuController@delete_item_detail')->name('item_detail.delete');
    Route::post('hit-kayu', 'KayuController@hit_kayu')->name('hit_kayu.get');
    Route::post('delete-kayu', 'KayuController@delete_data_kayu')->name('data_kayu.delete');
    Route::post('ganti-lahan', 'KayuController@ganti_lahan')->name('edit_lahan.simpan');

    // Datatable Kayu Sudah Dihitung
    Route::get('list-data', 'KayuController@list_data_kayu')->name('list_kayu.index');
    Route::get('datatable-kayu-bayar', 'KayuController@datatable_kayu_bayar')->name('kayu_bayar.datatable');
    Route::post('potongan-kayu-bayar', 'KayuController@potongan_kayu_bayar')->name('potongan.add');
    Route::get('edit-cek/{id}', 'KayuController@edit_cek')->name('hitungan.cek');
    Route::get('set-pembulatan', 'KayuController@set_pembulatan')->name('set_bulat.save');
    Route::get('nota/{id}', 'KayuController@nota')->name('kayu.print');
    Route::post('hapus_bayar', 'KayuController@hapus_bayar')->name('hs_bayar.delete');
    
   
    // Cek Hitungan Kayu
    Route::post('hit-kayu-update', 'KayuController@hit_kayu_update')->name('hit_kayu.update');
    Route::post('konfirmasi', 'KayuController@konfirmasi')->name('konfirmasi.cek');
    Route::post('batal-konfirmasi', 'KayuController@batal_konfirmasi')->name('batal_konfirmasi.cek');
});

Route::group(['prefix' => 'cek'], function () {
    // // Datatable Kayu Sudah Dihitung
    Route::get('list-data', 'CekKayuController@list_data_kayu')->name('list.index');
    Route::get('datatable-kayu-bayar', 'CekKayuController@datatable_kayu_bayar')->name('cek_kayu_bayar.datatable');
    Route::get('edit-cek/{id}', 'CekKayuController@edit_konfirm')->name('hit.cek');
    Route::post('batal-konfirmasi', 'CekKayuController@batal_konfirmasi')->name('batal_konfirm_admin.cek');
    Route::post('konfirmasi-banding', 'CekKayuController@konfirmasi_banding')->name('konfirmasi.banding');
    Route::post('edit-tanggal', 'CekKayuController@edit_tanggal')->name('simpan_edit.tanggal');
});
Route::group(['prefix' => 'pimpinan'], function () {

    Route::get('list-data', 'PimpinanController@list_kayu_pimpinan')->name('list_pimpinan.index');
    Route::get('datatable-kayu-bayar', 'PimpinanController@datatable_cek_pimpinan')->name('cek_bayar_pimpinan.datatable');
    Route::get('edit-cek/{id}', 'PimpinanController@konfirm_pimpinan')->name('pimpinan.cek');
    Route::post('konfirm-pimpinan', 'PimpinanController@konfirmasi_pimpinan')->name('pimpinan.konfirm');
    Route::post('batal-konfirmasi', 'PimpinanController@batal_konfirmasi')->name('batal_konfirmasi.cek');
    Route::post('simpan-edit-tanggal', 'PimpinanController@simpan_edit_tanggal')->name('edit.tanggal');
    Route::post('konfirmasi-pimpinan-banding', 'PimpinanController@konfirmasi_pimpinan_banding')->name('konfirmasi_pimpinan.banding');
});
Route::group(['prefix' => 'account'], function () {
    Route::get('index', 'AccountController@index')->name('account.index');
    Route::get('form-pengguna', 'AccountController@form_pengguna')->name('form_pengguna.index');
    Route::get('karyawan-list', 'AccountController@karyawan_list')->name('karyawan.list');
    Route::post('simpan', 'AccountController@simpan')->name('simpan.account');
    Route::get('account-datatable', 'AccountController@account_datatable')->name('datatable.account');
    Route::post('account-update', 'AccountController@update_account')->name('account.update');
    Route::post('account-delete', 'AccountController@account_delete')->name('account.delete');
});

Route::group(['prefix' => 'jurnal'], function () {
    Route::get('list-jurnal', 'JurnalController@list_jurnal')->name('list.jurnal');
    Route::post('jurnal-datatable', 'JurnalController@datatable_jurnal')->name('datatable.jurnal');
    Route::post('excel','JurnalController@excel_jurnal')->name('jurnal.excel');
    Route::post('datatable-seri','JurnalController@datatable_seri')->name('datatable.seri');
    Route::get('download','JurnalController@download')->name('download.excel');

});

Route::group(['prefix' => 'setting'], function () {
    Route::group(['prefix' => 'menu'], function () {
        Route::get('/', 'MenuController@index')->name('menu.index');
        Route::get('datatable', 'MenuController@datatable')->name('menu.datatable'); 
        Route::get('form', 'MenuController@form')->name('menu.form');
        Route::post('save', 'MenuController@save')->name('menu.save');
        Route::get('edit/{id}', 'MenuController@edit')->name('menu.edit');
        Route::post('delete', 'MenuController@delete')->name('menu.delete');
    });

    Route::group(['prefix' => 'akses'], function () {
        Route::get('/', 'MenuController@akses')->name('akses.index');
        Route::get('datatable', 'MenuController@datatable_akses')->name('akses.datatable');
        Route::get('form', 'MenuController@akses_form')->name('akses.form');
        Route::post('save', 'MenuController@akses_save')->name('akses.save');
        Route::post('delete', 'MenuController@akses_delete')->name('akses.delete');
        Route::get('list-akses', 'MenuController@list_akses')->name('list_akses.datatable');
        Route::get('list-fitur', 'MenuController@list_fitur')->name('list_fitur.datatable');
        Route::post('item-fitur', 'MenuController@item_fitur')->name('item_fitur.datatable');
        Route::post('delete-fitur', 'MenuController@delete_fitur')->name('item_fitur.delete');
    });

    Route::group(['prefix' => 'compare'], function () {
        Route::get('/', 'MenuController@compare')->name('compare.index');
        Route::post('setting-user', 'MenuController@setting_user')->name('setting.user');
        
    });
    
    //jabatan
    Route::group(['prefix' => 'jabatan'], function () {
        Route::get('/', 'JabatanController@index')->name('jabatan.index');
        Route::get('datatable', 'JabatanController@datatable')->name('jabatan.datatable');        
        Route::get('form', 'JabatanController@form')->name('jabatan.form');        
        Route::post('save', 'JabatanController@save')->name('jabatan.save');        
        Route::get('form-edit/{id}', 'JabatanController@form_edit')->name('jabatan.form_edit');        
        Route::post('update', 'JabatanController@update')->name('jabatan.update');        
        Route::post('delete', 'JabatanController@delete')->name('jabatan.delete');        
    });

    Route::group(['prefix' => 'stok'], function () {
        Route::get('grup_kayu', 'StokController@kayu_grup')->name('kayu.grup');
        Route::get('datatable-grup-kayu', 'StokController@kayu_grup_datatable')->name('grup_kayu.datatable');
        Route::get('kayu-grup-detail/{id_kayu}', 'StokController@kayu_grup_detail')->name('kayu_grup.detail');
        Route::post('kayu-grup-kayu-detail', 'StokController@kayu_grup_detail_data')->name('kayu_grup_detail.data');
        Route::get('qty-keluar', 'StokController@qty_keluar')->name('qty.keluar');
        Route::get('kayu-pertanggal', 'StokController@kayu_pertanggal')->name('grup_tanggal.datatable');

        Route::get('lahan-grup-detail/{id_kayu}', 'StokController@lahan_grup_detail')->name('lahan_grup.detail');
        Route::post('lahan-grup-detail-data', 'StokController@lahan_grup_detail_data')->name('lahan_grup_detail.data');


        //Rekap Kayu Masuk Harian
        Route::get('grup-harian', 'StokController@grup_harian')->name('harian.grup');
        Route::get('datatable-stok-harian', 'StokController@harian_stok_datatable')->name('stok_harian.datatable');
        Route::get('datatable-stok-detail/{tgl}', 'StokController@harian_stok_detail')->name('harian_stok.detail');
        Route::post('datatable-stok-detail-data', 'StokController@harian_stok_detail_data')->name('harian_stok_detail.data');
        Route::get('excel/{tgl}','StokController@excel_export')->name('stok.excel');

        // skajsklajksjaksjasj

        //Rekap Stok Kayu Masuk
        Route::get('grup-stok-harian', 'StokController@grup_stok_harian')->name('grup_stok.harian');
        Route::get('stok-datatable', 'StokController@stok_datatable')->name('stok.datatable');
        Route::get('stok-datatable-detail/{tgl}', 'StokController@stok_datatable_detail')->name('stok_detail.datatable');
        Route::post('stok-datatable-detail-data', 'StokController@stok_datatable_detail_data')->name('stok_data.detail');

        //Stok Kayu Keluar
        Route::post('stok-kayu-keluar', 'StokController@stok_kayu_keluar')->name('kayu.keluar');

        

        // Export Excel Stok Global Harian
        Route::get('excel-global/{tgl}','StokController@export_stok_excel')->name('export_stok.excel');
    });

    //Rekap Status Kayu Bayar
    Route::group(['prefix' => 'rekap'], function() {
        Route::get('grup-rekap-bayar', 'RekapBayarController@rekap_grup_bayar')->name('rekap.grup');
        Route::get('datatable-grup-bayar', 'RekapBayarController@bayar_grup_datatable')->name('grup_bayar.datatable');
        Route::get('datatable-grup-detail/{tgl}', 'RekapBayarController@bayar_grup_detail')->name('bayar_grup.detail');
        Route::post('datatable-grup-detail-data', 'RekapBayarController@bayar_grup_detail_data')->name('bayar_grup_detail.data');
        Route::post('konfirmasi-bayar', 'RekapBayarController@konfirmasi_bayar')->name('konfirmasi.bayar');
        Route::post('konfirmasi-batal', 'RekapBayarController@konfirmasi_batal')->name('konfirmasi.batal');

        Route::get('export-bayar/{tgl}','RekapBayarController@export_bayar')->name('export.bayar');
        
    });

    // Hitung Kayu Masuk Admin 2
    Route::group(['prefix' => 'kayuDua'], function () {
        Route::get('max-seri', 'KayuBandingController@ambil_seri')->name('ambil_seri.json');
        Route::get('hitungan_kayu', 'KayuBandingController@hitungan_kayu')->name('hitungan_kayu.index');
        Route::get('pilih-supplier', 'KayuBandingController@pilih_supplier')->name('pilih_supplier.datatable');
        Route::post('simpan-hitungan', 'KayuBandingController@simpan_hitungan')->name('hitungan.simpan');
        Route::post('ambil-kayu', 'KayuBandingController@ambil_kayu')->name('ambil_kayu.json');
        Route::post('ambil-item', 'KayuBandingController@ambil_item')->name('ambil_item.save');

        Route::post('ambil-harga', 'KayuBandingController@ambil_harga')->name('harga.ambil');
        Route::post('hapus-item', 'KayuBandingController@hapus_item')->name('item.delete');
        Route::post('ambil-hitungan-kayu', 'KayuBandingController@ambil_hitungan_kayu')->name('hitungan_kayu.ambil');
        Route::post('hapus-kayu', 'KayuBandingController@hapus_data_kayu')->name('kayu.hapus');
        Route::post('ganti-tanggal', 'KayuBandingController@ganti_tanggal')->name('simpan_edit.tanggal');
        Route::post('rubah-lahan', 'KayuBandingController@rubah_lahan')->name('rubah_lahan.simpan');

        Route::post('setting-pot-kubik', 'KayuBandingController@setting_pot_kubik')->name('setting.kubik');
        Route::post('ambil-pot-kubik', 'KayuBandingController@ambil_pot_kubik')->name('ambil.kubik');


        // Datatable kayu yang sudah dihitung admin (2)
        Route::get('kayu-sudah-dihitung', 'KayuBandingController@kayu_sudah_dihitung')->name('kayu_sudah_dihitung.index');
        Route::get('datatable-kayu-sudah-dihitung', 'KayuBandingController@datatable_kayu_sudah_dihitung')->name('kayu_sudah_dihitung.datatable');
        Route::post('potongan-kayu-dibayar', 'KayuBandingController@potongan_kayu_dibayar')->name('potongan.tambah');
        Route::get('edit-hitungan/{id}', 'KayuBandingController@edit_hitungan')->name('cek.hitungan');
        Route::get('setting-pembulatan', 'KayuBandingController@setting_pembulatan')->name('setting_bulat.save');
        Route::get('nota/{id}', 'KayuBandingController@nota_print')->name('nota.print');
        
        Route::post('hitungan_kayu_update', 'KayuBandingController@hitungan_kayu_update')->name('hitungan_kayu.update');
    });
});
// Hitung Kayu Keluar
    Route::group(['prefix' => 'kayukeluar'], function () {

        Route::get('grup-keluar', 'KayuKeluarController@grup_keluar')->name('grup_keluar.index');
        Route::post('kayu-keluar-datatable', 'KayuKeluarController@kayu_keluar_datatable')->name('kayu_keluar.datatable');
        Route::get('kayu-keluar-detail/{tgl}', 'KayuKeluarController@kayu_keluar_detail')->name('kayu_keluar.detail');
        Route::post('kayu-keluar-detail-data', 'KayuKeluarController@kayu_keluar_detail_data')->name('kayu_keluar_detail.data');
        Route::get('input-keluar', 'KayuKeluarController@input_keluar')->name('input_keluar.index');
        Route::post('simpan-kayu-keluar', 'KayuKeluarController@simpan_kayu_keluar')->name('kayu_keluar.simpan');

        Route::get('datatable-keluar', 'KayuKeluarController@datatable_keluar')->name('datatable.keluar');
        Route::post('simpan-edit-keluar', 'KayuKeluarController@simpan_edit_keluar')->name('kayu_edit.simpan');
        Route::post('hapus-keluar', 'KayuKeluarController@hapus_keluar')->name('keluar.delete');

});

// Set Stok Kayu Masuk
    Route::group(['prefix' => 'kayumasuk'], function () {
        
        Route::get('input-masuk', 'StokMasukController@input_masuk')->name('input_masuk.index');
        Route::post('simpan-kayu-masuk', 'StokMasukController@simpan_kayu_masuk')->name('kayu_masuk.simpan');
        Route::get('datatable-opname', 'StokMasukController@datatable_opname')->name('datatable.opname');
        Route::post('hapus-opname', 'StokMasukController@hapus_opname')->name('opname.delete');

        Route::post('simpan-edit-opname', 'StokMasukController@simpan_edit_opname')->name('opname_edit.simpan');
});

// Stok Baru
    Route::group(['prefix' => 'stokkayubaru'], function () {

        Route::get('excel', 'StokBaruController@excel_stok');
        Route::get('/', 'StokBaruController@stok_baru_coba')->name('stok_baru.coba');
        Route::get('detail/{param}', 'StokBaruController@stok_detail')->name('stok_detail.json');
        Route::get('update/{param}', 'StokBaruController@update_bayar');

        Route::get('global', 'StokBaruController@stok_global_baru')->name('stok_global.index');
        Route::get('global-datatable', 'StokBaruController@stok_global_baru_datatable')->name('stok_global.datatable');
        Route::get('global-detail/{cari}', 'StokBaruController@stok_global_baru_detail')->name('stok_global.detail');
        Route::post('global-detail-datatable', 'StokBaruController@stok_global_baru_detail_datatable')->name('stok_global_detail.datatable');
        // Route::post('kayu_grup_coba_datatable', 'StokBaruController@kayu_grup_coba_datatable')->name('kayu_grup_coba.datatable');

        Route::get('detail-tanggal-grup', 'StokBaruController@detail_tanggal_grup')->name('detail_tanggal.grup');
        Route::get('lahan-detail/{tgl}', 'StokBaruController@lahan_detail')->name('lahan.detail');
        Route::post('datatable-detail-lahan', 'StokBaruController@detail_lahan_datatable')->name('detail_lahan.datatable');

        //Stok Lahan Habis
        Route::get('lahan-habis', 'StokBaruController@lahan_habis')->name('lahan_habis.grup');
        Route::get('lahan-habis-pertanggal', 'StokBaruController@lahan_habis_pertanggal')->name('lahan_habis_pertanggal.datatable');
        Route::get('lahan-habis-detail/{tgl}', 'StokBaruController@lahan_habis_detail')->name('lahan_habis.detail');
        Route::post('lahan-habis-detail-data', 'StokBaruController@lahan_habis_detail_data')->name('lahan_habis_detail.data');
        
        Route::get('export-stok-global-harian/{tgl}','StokBaruController@export_stok_global_harian')->name('export_stok_global.excel');
        Route::post('export-stok-global-harian-new','StokBaruController@export_stok_global_harian_new')->name('export_stok_global_new.excel');
        Route::get('unduh-file/{tgl}','StokBaruController@unduh_file')->name('export_stok_global_new.excel_unduh');
    });

    Route::group(['prefix' => 'dashboardstok'], function () {
        
        Route::get('dashboard', 'DashboardStokController@dashboard')->name('dashboard_admin.index');
        Route::get('stok-dashboard', 'DashboardStokController@stok_dashboard')->name('stok.dashboard');
        
    });

    Route::group(['prefix' => 'efisiensi'], function ()
    {
        Route::get('index-efisensi', 'EfisiensiController@index_efisiensi')->name('efisiensi_index');
        Route::post('pilih-lahan', 'EfisiensiController@pilih_lahan')->name('pilih.lahan');
    });

    Route::group(['prefix' => 'meranti'], function ()
    {
        Route::get('form-meranti', 'MerantiController@form_meranti')->name('form_meranti.index');
        Route::post('simpan-data', 'MerantiController@simpan_data')->name('data.simpan');
        Route::post('take-kayu', 'MerantiController@take_kayu')->name('take_kayu.json');
        Route::post('simpan-item', 'MerantiController@simpan_item')->name('item.simpan');
        Route::post('hapus-item', 'MerantiController@hapus_item')->name('detail_item.hapus');
        Route::post('hitung-kayu', 'MerantiController@hitung_kayu')->name('hitung_kayu.take');
        Route::post('tambah-poin', 'MerantiController@tambah_poin')->name('poin.add');
        Route::post('hapus-detil-kayu', 'MerantiController@hapus_detil_kayu')->name('detil_kayu.delete');

        Route::get('list-kayu-meranti', 'MerantiController@list_kayu_meranti')->name('list_meranti.index');
        Route::get('datatable_kayu_meranti', 'MerantiController@datatable_kayu_meranti')->name('kayu_meranti.datatable');
        Route::get('edit-hitungan/{id}', 'MerantiController@edit_hitungan')->name('hitungan.edit');
        Route::post('hitung-update', 'MerantiController@hitung_update')->name('hitung_kayu.update');
        
    });



    Route::get('coba', 'EfisiensiController@ambil_kupasan')->name('ambil_kupasan.index');
    Route::get('cebox', 'EfisiensiController@kupasanQ');


   



