<?php

use Illuminate\Support\Facades\Route;

/*jymymymym
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('beranda', 'BerandaController@index')->name('beranda.index');
Route::group(['prefix' => 'barang'], function(){
//        Route::get('/cari','BarangController@cari')->name('barang.cari');
//        Route::get('/cari_id','BarangController@cari_id')->name('barang.cari_id');
       Route::get('/','BarangController@index')->name('barang.index');
       
       Route::get('/hapus/{id}','BarangController@hapus')->name('barang.sudah_dihapus');
       Route::get('/hapus','BarangController@lamanHapus')->name('barang.pilih_hapus');

       Route::get('/ubah','BarangController@ubah')->name('barang.pilih_edit');
       Route::get('/edit/{id}','BarangController@edit')->name('barang.edit_form');
       Route::post('/update','BarangController@update')->name('barang.sudah_edit');
       
       
       Route::get('/tambahbarang','BarangController@tambahbarang')->name('barang.tambah');
       Route::post('/store','BarangController@store')->name('barang.simpan');
       
       Route::get('/tambahstok','BarangController@tambahstok')->name('stok_barang.pilih_tambah');
       Route::get('/penambahanstok/{id}','BarangController@penambahanstok')->name('stok_barang.penambahan');
       Route::post('/updatestok','BarangController@updatestok')->name('stok_barang.sudah_ditambah');
       
       Route::get('/kurangistok', 'BarangController@kurangistok')->name('stok_barang.pilih_kurang');
       Route::get('/penguranganstok/{id}','BarangController@penguranganstok')->name('stok_barang.pengurangan');
       Route::post('/mengurangistok','BarangController@mengurangistok')->name('stok_barang.sudah_dikurang');
       
       
       

       Route::get('/print', 'BarangController@print')->name('barang.print');

       Route::get('/export_excel', 'BarangController@export_excel')->name('barang.export');
       Route::post('/import_excel', 'BarangController@import_excel')->name('barang.import');
});
    


